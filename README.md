# Summary
This repo contains the solution to adform challenges. To make it easier to test I've added commands to run it on Docker.

The challenge document is placed in the root directory: "Homework assignment_AIR group.docx"

There are two directories for each challenge:

- sql - for "SQL challenge" code
- csharp - for "C# challenge" code

The code can be browsed online at https://bitbucket.org/pranas_lukavicius/adform-challenge/src

# Getting the code

The easiest way to get the code is by cloning the samples repository with git, using the following instructions.

```console
git clone https://bitbucket.org/pranas_lukavicius/adform-challenge.git
```

# SQL challenge solution

All code for a SQL challenge is under the \sql directory. Solution queries are in [sql\solution_queries.sql] file.

## Build and run the postgres server

I've put a simple dockerfile to build the docker image for postgresql server:
```
FROM library/postgres:9.6.6-alpine
COPY 0_init.sql /docker-entrypoint-initdb.d/
COPY 1_data.sql /docker-entrypoint-initdb.d/
```
I chose an alpine version of postgres as base image because it's lightweight to download. Dockerfile instructs to copy the initialization SQL files to /docker-entrypoint-initdb.d/ on build. These SQL files will be executed in container on startup. \[0_init.sql] file creates the database. And \[1_data.sql] populates database with sample data.

You can build and run the postgres server in Docker using the following commands. The instructions assume that you are in the root of the repository.

```console
cd sql
docker build --rm -t adform-sql-challenge:latest .
docker run --rm --name adform-sql-challenge-server -e POSTGRES_PASSWORD=Password1 -p 5433:5432 -d adform-sql-challenge:latest
```

Once the postgres server is running you can connect to it on {docker-host-ip}:5433. Or better yet follow instructions in the next section to run commands in psql in docker.

## Run solution queries on postgres server

### Run a new container with psql that connects to postgres server 
```console
docker run -it --rm --link adform-sql-challenge-server:postgres-srv -e PGPASSWORD=Password1 library/postgres:9.6.6-alpine psql -h postgres-srv -U postgres -d adform
```

Type "\q" in psql to exit when you're done. Container will stop and be deleted imeadetely as well (--rm option).

### Solution to 1st SQL challenge

Paste the following SQL command in psql shell:

```console
select coalesce(clientid::text,'n/a') as client, coalesce(sum(total),0) as total
from orders
full outer join users
on users.userid = orders.userid
group by users.clientid;
```

Output:
```console
 client |  total
--------+---------
 n/a    |   10.00
 5      | 1100.00
 6      |       0
(3 rows)
```

### Solution to 2nd SQL challenge

Paste the following SQL command in psql shell:

```console
select coalesce(clientid::text,'n/a') as client, coalesce(sum(total),0) as total
from orders
full outer join users
on users.userid = orders.userid
group by users.clientid
order by total desc
limit 2;
```

Output:
```console
 client |  total
--------+---------
 5      | 1100.00
 n/a    |   10.00
(2 rows)
```

## Clean docker images and containers

```console
docker rm -f adform-sql-challenge-server
docker rmi -f adform-sql-challenge:latest
docker rmi -f library/postgres:9.6.6-alpine
```

# C# challenge solution

All code for a C# challenge is under the \csharp directory.

## Build the docker image for dotnet core console app

```console
cd csharp
docker build --rm -t adform-csharp-challenge:latest .
```

## Solution to 1st c# challenge

Code:
```csharp
    var dailyImpressions = repClient.GetDailyImpressionsForThisYear(token);
    var cal = new GregorianCalendar();
    var weeklyImpressions = dailyImpressions
        .GroupBy(g => cal.GetWeekOfYear(g.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday))
        .Select(p => new WeeklyImpressions { Week = p.Key, Impressions = p.Select(r => r.Value).Sum(), DaysInWeek = p.Count() });
```

Run console app:
```console
docker run --rm --name adform-csharp-challenge-inst adform-csharp-challenge:latest weekly-impressions
```

Output:
```console
[{"Week":1,"Impressions":200,"DaysInWeek":1},{"Week":2,"Impressions":1663,"DaysInWeek":7},{"Week":3,"Impressions":1400,"DaysInWeek":7},{"Week":4,"Impressions":1407,"DaysInWeek":7},{"Week":5,"Impressions":1684,"DaysInWeek":7},{"Week":6,"Impressions":1460,"DaysInWeek":7},{"Week":7,"Impressions":1463,"DaysInWeek":7},{"Week":8,"Impressions":1247,"DaysInWeek":6},{"Week":9,"Impressions":943,"DaysInWeek":7},{"Week":10,"Impressions":1348,"DaysInWeek":7},{"Week":11,"Impressions":1444,"DaysInWeek":7},{"Week":12,"Impressions":1477,"DaysInWeek":7},{"Week":13,"Impressions":2625,"DaysInWeek":7},{"Week":14,"Impressions":2494,"DaysInWeek":7},{"Week":15,"Impressions":2159,"DaysInWeek":7},{"Week":16,"Impressions":2179,"DaysInWeek":7},{"Week":17,"Impressions":2177,"DaysInWeek":7},{"Week":18,"Impressions":1898,"DaysInWeek":7},{"Week":19,"Impressions":2173,"DaysInWeek":7},{"Week":20,"Impressions":2163,"DaysInWeek":7},{"Week":21,"Impressions":2173,"DaysInWeek":7},{"Week":22,"Impressions":2164,"DaysInWeek":7},{"Week":23,"Impressions":2186,"DaysInWeek":7},{"Week":24,"Impressions":2180,"DaysInWeek":7},{"Week":25,"Impressions":2161,"DaysInWeek":7},{"Week":26,"Impressions":2156,"DaysInWeek":7},{"Week":27,"Impressions":2000,"DaysInWeek":7},{"Week":28,"Impressions":3650,"DaysInWeek":7},{"Week":29,"Impressions":1818,"DaysInWeek":7},{"Week":30,"Impressions":2473,"DaysInWeek":7},{"Week":31,"Impressions":2158,"DaysInWeek":7},{"Week":32,"Impressions":2612,"DaysInWeek":7},{"Week":33,"Impressions":1996,"DaysInWeek":7},{"Week":34,"Impressions":931,"DaysInWeek":6},{"Week":35,"Impressions":1092,"DaysInWeek":7},{"Week":36,"Impressions":1099,"DaysInWeek":7},{"Week":37,"Impressions":2294,"DaysInWeek":7},{"Week":38,"Impressions":2146,"DaysInWeek":7},{"Week":39,"Impressions":2161,"DaysInWeek":7},{"Week":40,"Impressions":2159,"DaysInWeek":7},{"Week":41,"Impressions":2154,"DaysInWeek":7},{"Week":42,"Impressions":1841,"DaysInWeek":6},{"Week":43,"Impressions":1849,"DaysInWeek":6},{"Week":44,"Impressions":2517,"DaysInWeek":7},{"Week":45,"Impressions":2145,"DaysInWeek":7},{"Week":46,"Impressions":2138,"DaysInWeek":7},{"Week":47,"Impressions":2156,"DaysInWeek":7},{"Week":48,"Impressions":611,"DaysInWeek":2}]
```

## Solution to 2nd c# challenge

For anomalies detection I chose to compare the values with the median. If value is 3 times greater or lower than the median then I treat it as anomaly.

Code:
```csharp
    var dailyBidRequests = repClient.GetDailyBidRequestsForThisYear(token);
    var median = Median(dailyBidRequests.Select(p => p.Value));
    var anomalies = dailyBidRequests
        .Where(p => p.Value > median * 3 || p.Value < median / 3)
        .Select(p => new Anomaly { Date = p.Date, Value = p.Value })
        .ToList();
```


Run console app:
```console
docker run --rm --name adform-csharp-challenge-inst adform-csharp-challenge:latest anomalies
```

Output:
```console
{"Median":300,"Values":[{"Date":"2017-04-13T00:00:00","Value":915},{"Date":"2017-07-03T00:00:00","Value":2704},{"Date":"2017-07-11T00:00:00","Value":902},{"Date":"2017-07-13T00:00:00","Value":41}]}
```

## Run integration tests in docker

Integration tests checks that client calls to Adform API works

```console
docker build --rm -t adform-csharp-challenge-tests:latest -f Dockerfile.Tests .
docker run --rm --name adform-csharp-challenge-tests-inst adform-csharp-challenge-tests:latest --filter "Category=Integration"
```

## Clean docker images and containers

```console
docker rmi -f adform-csharp-challenge-tests:latest
docker rmi -f adform-csharp-challenge:latest
docker rmi -f microsoft/dotnet:2.0-sdk
docker rmi -f microsoft/dotnet:2.0-runtime
```