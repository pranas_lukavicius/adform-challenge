using System;
using Xunit;
using Xunit.Abstractions;
using Reporting;

namespace Reporting.Tests
{
    public class AuthenticationApiClientTests
    {
        [Fact]
        [Trait("Category", "Integration")]
        public void GetToken_MissingParameter_ThrowsArgumentException()
        {
            var client = new AuthenticationApiClient();

            {
                var req = new TokenRequest() { grant_type = null, client_id = "", client_secret = "", scope = "" };
                ArgumentException ex = Assert.Throws<ArgumentException>(() => client.GetToken(req));
                Assert.Equal("grant_type", ex.ParamName);
            }

            {
                var req = new TokenRequest() { grant_type = "", client_id = null, client_secret = "", scope = "" };
                ArgumentException ex = Assert.Throws<ArgumentException>(() => client.GetToken(req));
                Assert.Equal("client_id", ex.ParamName);
            }

            {
                var req = new TokenRequest() { grant_type = "", client_id = "", client_secret = null, scope = "" };
                ArgumentException ex = Assert.Throws<ArgumentException>(() => client.GetToken(req));
                Assert.Equal("client_secret", ex.ParamName);
            }

            {
                var req = new TokenRequest() { grant_type = "", client_id = "", client_secret = "", scope = null };
                ArgumentException ex = Assert.Throws<ArgumentException>(() => client.GetToken(req));
                Assert.Equal("scope", ex.ParamName);
            }
        }

        [Fact]
        [Trait("Category", "Integration")]
        public void GetToken_InvalidCredentials_ThrowsException()
        {
            var client = new AuthenticationApiClient();
            var req = new TokenRequest() { grant_type = "", client_id = "", client_secret = "", scope = "" };
            Exception ex = Assert.Throws<Exception>(() => client.GetToken(req));
            Assert.Equal("AuthenticationApiClient.0002", ex.Data["CODE"]);
        }

        [Fact]
        [Trait("Category", "Integration")]
        public void GetToken_ValidCredentials_ReturnsToken()
        {
            var client = new AuthenticationApiClient();
            var req = new TokenRequest()
            {
                grant_type = "client_credentials",
                client_id = "sellside.apiteam@tests.adform.com",
                client_secret = "xPDUpHFZHuobERbKVjVxPujndfyg4C6KLDItwLwK",
                scope = "https://api.adform.com/scope/eapi"
            };
            var token = client.GetToken(req);

            Assert.True(token?.access_token?.Length > 0);
        }
    }
}
