using System;
using System.Linq;
using Xunit;
using Xunit.Abstractions;
using Reporting;

namespace Reporting.Tests
{
    public class SellerReportsApiClientTests
    {
        private Token token;

        public SellerReportsApiClientTests()
        {
            token = Authenticate();
        }

        [Fact]
        [Trait("Category", "Integration")]
        public void GetDailyBidRequestsForThisYear_ValidRequest_ReturnsResult()
        {
            var client = new SellerReportingApiClient();
            var resp = client.GetDailyBidRequestsForThisYear(token);
            Assert.True(resp?.Count >= 0);
        }

        [Fact]
        [Trait("Category", "Integration")]
        public void GetDailyImpressionsForThisYear_ValidRequest_ReturnsResult()
        {
            var client = new SellerReportingApiClient();
            var resp = client.GetDailyImpressionsForThisYear(token);
            Assert.True(resp?.Count >= 0);
        }

        private Token Authenticate()
        {
            var req = new TokenRequest()
            {
                grant_type = "client_credentials",
                client_id = "sellside.apiteam@tests.adform.com",
                client_secret = "xPDUpHFZHuobERbKVjVxPujndfyg4C6KLDItwLwK",
                scope = "https://api.adform.com/scope/eapi"
            };
            var client = new AuthenticationApiClient();
            return client.GetToken(req);
        }
    }
}
