﻿using System;
using Newtonsoft.Json;

namespace Reporting
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args?.Length != 1 || args[0] != "weekly-impressions" && args[0] != "anomalies")
            {
                Console.WriteLine("{ \"ERROR\": \"Argument missing. Specify 'weekly-impressions' or 'anomalies' as the first argument.\" }");
            }

            var authClient = new AuthenticationApiClient();
            var repClient = new SellerReportingApiClient();
            var dataAnalyzer = new DataAnalyzer(authClient, repClient);
            if (args[0] == "weekly-impressions")
            {
                var weeklyImpressions = dataAnalyzer.GetWeeklyImpressions();
                Console.WriteLine(JsonConvert.SerializeObject(weeklyImpressions));
            }
            if (args[0] == "anomalies")
            {
                var anomalies = dataAnalyzer.GetAnomalies();
                Console.WriteLine(JsonConvert.SerializeObject(anomalies));
            }
        }
    }
}
