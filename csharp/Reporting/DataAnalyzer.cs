using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Reporting
{
    public class DataAnalyzer
    {
        private AuthenticationApiClient _authClient;
        private SellerReportingApiClient _repClient;
        public DataAnalyzer(AuthenticationApiClient authClient, SellerReportingApiClient repClient)
        {
            _authClient = authClient;
            _repClient = repClient;
        }

        public List<WeeklyImpressions> GetWeeklyImpressions()
        {
            var token = Authenticate();
            var dailyImpressions = _repClient.GetDailyImpressionsForThisYear(token);
            var cal = new GregorianCalendar();
            var weeklyImpressions = dailyImpressions
                .GroupBy(g => cal.GetWeekOfYear(g.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday))
                .Select(p => new WeeklyImpressions { Week = p.Key, Impressions = p.Select(r => r.Value).Sum(), DaysInWeek = p.Count() })
                .ToList();
            return weeklyImpressions;
        }

        public Anomalies GetAnomalies()
        {
            var token = Authenticate();
            var dailyBidRequests = _repClient.GetDailyBidRequestsForThisYear(token);
            var median = Median(dailyBidRequests.Select(p => p.Value));
            var anomalies = dailyBidRequests
                .Where(p => p.Value > median * 3 || p.Value < median / 3)
                .Select(p => new Anomaly { Date = p.Date, Value = p.Value })
                .ToList();
            var result = new Anomalies { Median = median, Values = anomalies };
            return result;
        }

        public Token Authenticate()
        {
            var req = new TokenRequest()
            {
                grant_type = "client_credentials",
                client_id = "sellside.apiteam@tests.adform.com",
                client_secret = "xPDUpHFZHuobERbKVjVxPujndfyg4C6KLDItwLwK",
                scope = "https://api.adform.com/scope/eapi"
            };
            var token = _authClient.GetToken(req);
            return token;
        }

        long Median(IEnumerable<long> xs)
        {
            var ys = xs.OrderBy(x => x).ToList();
            double mid = (ys.Count - 1) / 2.0;
            return (ys[(int)(mid)] + ys[(int)(mid + 0.5)]) / 2;
        }
    }

    public class Anomalies
    {
        public long Median { get; set; }
        public List<Anomaly> Values { get; set; }
    }

    public class Anomaly
    {
        public DateTime Date { get; set; }
        public long Value { get; set; }
    }

    public class WeeklyImpressions
    {
        public int Week { get; set; }
        public long Impressions { get; set; }
        public int DaysInWeek { get; set; }
    }
}