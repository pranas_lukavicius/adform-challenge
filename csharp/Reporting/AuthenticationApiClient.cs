using System;
using System.Net;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Validation;

namespace Reporting
{
    public class AuthenticationApiClient
    {
        const string BaseUrl = "https://id.adform.com/";

        public T Execute<T>(RestRequest request) where T : new()
        {
            var client = new RestClient();
            client.BaseUrl = new System.Uri(BaseUrl);
            var response = client.Execute<T>(request);

            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                var exception = new Exception(message, response.ErrorException);
                exception.Data["CODE"] = "AuthenticationApiClient.0001";
                throw exception;
            }
            if(response.StatusCode != HttpStatusCode.OK){
                string message = $"Token request error. Response: {response.Content}.";
                var exception = new Exception(message);
                exception.Data["CODE"] = "AuthenticationApiClient.0002";
                throw exception;
            }
            return response.Data;
        }

        public Token GetToken(TokenRequest req){
            Require.Argument("grant_type", req.grant_type);
            Require.Argument("client_id", req.client_id);
            Require.Argument("client_secret", req.client_secret);
            Require.Argument("scope", req.scope);

            var request = new RestRequest(Method.POST);
            request.Resource = "sts/connect/token";

            request.AddParameter("grant_type", req.grant_type);
            request.AddParameter("client_id", req.client_id);
            request.AddParameter("client_secret", req.client_secret);
            request.AddParameter("scope", req.scope);

            return Execute<Token>(request);
        }
    }

    public class TokenRequest
    {
        public string grant_type { get; set; }
        public string client_id { get; set; }
        public string client_secret { get; set; }
        public string scope { get; set; }
    }

    public class Token
    {
        public string access_token { get; set; }
        public string expires_in { get; set; }
        public string token_type { get; set; }
    }
}
