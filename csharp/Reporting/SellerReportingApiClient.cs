using System;
using System.Net;
using System.Linq;
using System.Collections.Generic;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Validation;

namespace Reporting
{
    public class SellerReportingApiClient
    {
        const string BaseUrl = "https://api.adform.com/";

        public List<DailyMetric> GetDailyImpressionsForThisYear(Token token)
        {
            var reportRequest = new ReportDataRequest()
            {
                dimensions = new[] { "date" },
                metrics = new[] { "impressions" },
                filter = new PresetIntervalDateFilter()
                {
                    Date = "thisYear"
                }
            };
            return GetReportData(token, reportRequest);
        }

        public List<DailyMetric> GetDailyBidRequestsForThisYear(Token token)
        {
            var reportRequest = new ReportDataRequest()
            {
                dimensions = new[] { "date" },
                metrics = new[] { "bidRequests" },
                filter = new PresetIntervalDateFilter()
                {
                    Date = "thisYear"
                }
            };
            return GetReportData(token, reportRequest);
        }

        private List<DailyMetric> GetReportData(Token token, ReportDataRequest reportRequest)
        {
            Require.Argument("access_token", token.access_token);

            var request = new RestRequest(Method.POST);
            request.Resource = "v1/reportingstats/publisher/reportdata";

            request.AddHeader("ticket", token.access_token);
            request.AddJsonBody(reportRequest);
            var resp = Execute<ReportResponse>(request);
            return resp?.reportData?.rows?.Select(p => new DailyMetric { Date = (DateTime)p[0], Value = (long)p[1] }).ToList();
        }

        private T Execute<T>(RestRequest request) where T : new()
        {
            var client = new RestClient();
            client.BaseUrl = new System.Uri(BaseUrl);
            client.AddHandler("application/json", NewtonsoftJsonSerializer.Default);
            request.JsonSerializer = NewtonsoftJsonSerializer.Default;
            var response = client.Execute<T>(request);

            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                var exception = new Exception(message, response.ErrorException);
                exception.Data["CODE"] = "ReportsApiClient.0001";
                throw exception;
            }
            if (response.StatusCode != HttpStatusCode.OK)
            {
                string message = $"Error. Response: StatusCode={response.StatusCode}, Content={response.Content}.";
                var exception = new Exception(message);
                exception.Data["CODE"] = "ReportsApiClient.0002";
                throw exception;
            }
            return response.Data;
        }
    }

    public class DailyMetric
    {
        public DateTime Date { get; set; }
        public long Value { get; set; }
    }

    class ReportDataRequest
    {
        public string[] dimensions { get; set; }
        public string[] metrics { get; set; }
        public object filter { get; set; }
    }

    class PresetIntervalDateFilter
    {
        public string Date { get; set; }
    }

    class ReportResponse
    {
        public ReportData reportData { get; set; }
    }

    class ReportData
    {
        public List<List<object>> rows { get; set; }
    }
}
