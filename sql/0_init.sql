CREATE DATABASE adform;

\connect adform;

create table users (
	UserID integer,
	UserName text,
	ClientID integer
);

create table orders (
	OrderID integer,
	Total decimal(12,2),
	UserID integer
);
