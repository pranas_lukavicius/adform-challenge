\connect adform;

INSERT INTO users(userid, username, clientid) 
values	(1, 'Audis', 5),
		(2, 'Balius', 5),
		(3, 'Diena', 6);


INSERT INTO orders(orderid, total, userid) 
values 	(33, 100, 1),
		(55, 1000, 2),
		(77, 10, 7);
