-- query for 1st sql challenge
select coalesce(clientid::text,'n/a') as client, coalesce(sum(total),0) as total
from orders
full outer join users
on users.userid = orders.userid
group by users.clientid;

-- query for 2nd sql challenge
select coalesce(clientid::text,'n/a') as client, coalesce(sum(total),0) as total
from orders
full outer join users
on users.userid = orders.userid
group by users.clientid
order by total desc
limit 2;